﻿using System;
using System.Diagnostics;
using System.Drawing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LotteryService.FrontEnd.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using QRCoder;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;

namespace LotteryService.FrontEnd.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IWebHostEnvironment _env;

        public HomeController(ILogger<HomeController> logger, IWebHostEnvironment env)
        {
            _logger = logger;
            _env = env;
        }

        [HttpGet]
        public IActionResult Code2()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Code2(string img)
        {
            SmtpClient client = new SmtpClient("smtp.yandex.ru", 587);
            var emailfrom = "JumpingOfficial@yandex.ru";
            var emailpwd = "IStudyService";
            client.Credentials = new NetworkCredential(emailfrom, emailpwd);
            client.EnableSsl = true;
            MailAddress from = new MailAddress(emailfrom, "Jumping Team",
            System.Text.Encoding.UTF8);
            MailAddress to = new MailAddress("Lexs200610@yandex.ru");
            MailMessage message = new MailMessage(from, to);
            message.Body = "";
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Subject = "Визитная карточка";
            message.SubjectEncoding = System.Text.Encoding.UTF8;

            string fileName = "BusinessCard_"+ Guid.NewGuid();
            string path = "wwwroot/logo/";
            string fileNameWitPath = Path.Combine(path, fileName + ".png");

            using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    byte[] data = Convert.FromBase64String(img);
                    bw.Write(data);
                    bw.Close();
                }
                fs.Close();
            }

            Attachment attachment = new Attachment(fileNameWitPath);
            message.Attachments.Add(attachment);

            string a = "BEGIN:VCARD" + "\r\n";
            a += "VERSION:3.0" + "\r\n";
            a += "N;CHARSET=UTF-8:Попов;Алексей;Сергеевич;" + "\r\n";
            a += "FN;CHARSET=UTF-8:Алексей Сергеевич" + "\r\n";
            a += "TITLE;CHARSET=UTF-8:Ведущий инженер-программист Ведущий инженер-программист" + "\r\n";
            a += "TEL;TYPE=work:+7(3494) 96-61-20" + "\r\n";
            a += "TEL;TYPE=cell:+7(922)486-94-56" + "\r\n";
            a += "EMAIL;CHARSET=UTF-8:Lexs200610@yandex.ru" + "\r\n";
            a += "ADR;CHARSET=UTF-8:;;д. 9;ул. Геологоразведчиков;г. Новый Уренгой;ЯНАО;Россия;629306" + "\r\n";
            a += "END:VCARD";

            string path2 = Path.Combine(path, fileName + ".vcf");
            if (!System.IO.File.Exists(path2))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(path2))
                {
                    sw.WriteLine(a);
                };
            }

            message.Attachments.Add(new Attachment(path2));


            client.Send(message);
            System.IO.File.Delete(fileNameWitPath);
            System.IO.File.Delete(path2);
            return View();
        }



        [HttpGet]
        public IActionResult ActionQrCode()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ActionQrCode(QRModel qr)
        {

            QRCodeGenerator ObjQr = new QRCodeGenerator();

            QRCodeData qrCodeData = ObjQr.CreateQrCode(qr.Message, QRCodeGenerator.ECCLevel.Q);

            Bitmap bitMap = new QRCode(qrCodeData).GetGraphic(20);

            using (MemoryStream ms = new MemoryStream())

            {

                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                byte[] byteImage = ms.ToArray();

                ViewBag.Url = "data:image/png;base64," + Convert.ToBase64String(byteImage);

            }

            return View();

        }

        public IActionResult Index()
        {
            return View();
        }


        public IActionResult Code()
        {
            var filename = "logo.png";
            var directory = Path.Combine(_env.WebRootPath, "EmployeePhotos");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            var file = Path.Combine(_env.WebRootPath, "EmployeePhotos", filename);


            //byte[] imageData;

            //var qrWriter = new ZXing.BarcodeWriter<System.Drawing.Bitmap>
            //{
            //    Format = BarcodeFormat.QR_CODE,
            //    Options = new ZXing.Common.EncodingOptions { Height = 100, Width = 100, Margin = 0 },
            //    Renderer = new ZXing.Rendering.BitmapRenderer()
            //};

            //Bitmap bitmap = qrWriter.Write("hello");
            //using (var ms = new System.IO.MemoryStream())
            //using (System.Drawing.Bitmap pixelData = qrWriter.Write("hello"))
            //{
            //    pixelData.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            //    imageData = ms.ToArray();
            //}


            //BarcodeWriterPixelData barcodeWriter = new BarcodeWriterPixelData<Bitmap>();
            //EncodingOptions encodingOptions = new EncodingOptions()
            //{
            //    Width = 300,
            //    Height = 300,
            //    Margin = 20,
            //    PureBarcode = false
            //};
            //encodingOptions.Hints.Add(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            //barcodeWriter.Renderer = new BitmapRenderer();
            //barcodeWriter.Options = encodingOptions;
            //barcodeWriter.Format = BarcodeFormat.QR_CODE;
            //Bitmap bitmap = barcodeWriter.Write("Hello");
            //Bitmap logo = new Bitmap(file);
            //Graphics g = Graphics.FromImage(bitmap);
            //g.DrawImage(logo, new Point((bitmap.Width - logo.Width)/2,(bitmap.Height - logo.Height)/2));


            return View();  
        }

        public IActionResult Privacy()  
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}