﻿using LotteryService.FrontEnd.Models.DTO;
using LotteryService.FrontEnd.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace LotteryService.FrontEnd.Controllers
{
    /// <summary>
    /// Управление подразделениями Общества.
    /// </summary>
    public class DepartmentController : Controller
    {
        private readonly IDepartmentService _departmentService;

        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }
        
        public async Task<IActionResult> Index()
        {
            return View(await _departmentService.GetAllDepartments());
        }
        
        
        public async Task<IActionResult> Details(Guid id)
        {
            return View(await _departmentService.GetDepartmentById(id));
        }
        
        [HttpGet]
        public  IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Создать подразделение 
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateOrUpdateDepartmentDto department)
        {
            await _departmentService.AddDepartmentAsync(department);
            return RedirectToAction(nameof(Index));
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            return View(await _departmentService.GetDepartmentForUpdateById(id));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, CreateOrUpdateDepartmentDto department)
        {
            await _departmentService.UpdateDepartmentAsync(id, department);
            return RedirectToAction(nameof(Index));
        }
        
        [HttpGet]
        public async Task<IActionResult> DepartmentForDelete(Guid id)
        {
            return View(await _departmentService.GetDepartmentById(id));
        }

        
        public async Task<IActionResult> Delete(Guid id)
        {
            await _departmentService.RemoveDepartmentAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
