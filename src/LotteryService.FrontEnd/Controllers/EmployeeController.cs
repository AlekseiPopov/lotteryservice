﻿using LotteryService.FrontEnd.Models.DTO;
using LotteryService.FrontEnd.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using LotteryService.FrontEnd.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace LotteryService.FrontEnd.Controllers
{
    /// <summary>
    /// Управление сотрудниками Общества.
    /// </summary>
    
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService _employeeService;
        private readonly IDepartmentService _departmentService;
        private readonly IWebHostEnvironment _env;

        public EmployeeController(IEmployeeService employeeService, IDepartmentService departmentService, IWebHostEnvironment env)
        {
            _employeeService = employeeService;
            _departmentService = departmentService;
            _env = env;
        }
        
        public async Task<IActionResult> Index()
        {
            var employees = await _employeeService.GetAllEmployeesWithDepartments();
            return View(employees);
        }
        
        [HttpGet]
        public  IActionResult Search()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Search(int number)
        {
            var emp = await _employeeService.GetEmployeeByPersonalNumber(number);
            return PartialView("_Search", emp);
        }

        public async Task<IActionResult> Details(Guid id)
        {
            return View(await _employeeService.GetEmployeeWithDepartmentById(id));
        }
        
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var departments = await _departmentService.GetAllDepartments();
            var listDepartments = new List<SelectListItem>();

            foreach (DepartmentResponseDto dep in departments)
            {
                var item = new SelectListItem {Text = dep.Name, Value = dep.Id.ToString()};
                listDepartments.Add(item);
            }
            ViewBag.Departments = listDepartments;
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Create(CreateOrUpdateEmployeeDto employee)
        {
            await _employeeService.AddEmployeeAsync(await SavePhoto(employee));
            return RedirectToAction(nameof(Index));
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            var departments = await _departmentService.GetAllDepartments();
            var listDepartments = new List<SelectListItem>();

            foreach (DepartmentResponseDto dep in departments)
            {
                var item = new SelectListItem {Text = dep.Name, Value = dep.Id.ToString()};
                listDepartments.Add(item);
            }

            ViewBag.Departments = listDepartments;
            return View(await _employeeService.GetEmployeeForUpdate(id));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, CreateOrUpdateEmployeeDto employee)
        {
            await _employeeService.UpdateEmployeeAsync(id, await SavePhoto(employee));
            return RedirectToAction(nameof(Index));
        }
        
        [HttpGet]
        public async Task<IActionResult> EmployeeForDelete(Guid id)
        {
            return View(await _employeeService.GetEmployeeById(id));
        }
        
       
        public async Task<IActionResult> Delete(Guid id)
        {
            await _employeeService.RemoveEmployeeAsync(id);
            return RedirectToAction(nameof(Index));
        }

        public async Task<CreateOrUpdateEmployeeDto> SavePhoto(CreateOrUpdateEmployeeDto employee)
        {
            if (employee.Photo != null)
            {
                var extension = "." + employee.Photo.FileName.Split('.')[employee.Photo.FileName.Split('.').Length - 1];
                var filename = Guid.NewGuid().ToString() + '_' + employee.SecondName + '_' + employee.Name + '_' +
                               employee.ThirdName + extension;
                var directory = Path.Combine(_env.WebRootPath, "EmployeePhotos");
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(_env.WebRootPath, "EmployeePhotos", filename);

                await using (var stream = new FileStream(path, FileMode.Create))
                {
                    await employee.Photo.CopyToAsync(stream);
                }
                employee.PhotoPath = filename;
            }
            else
            {
                employee.PhotoPath = "";
            }

            return employee;
        }
    }
}
