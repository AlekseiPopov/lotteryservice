﻿namespace LotteryService.FrontEnd.Models
{
    public interface IDatabaseInitializer
    {
        void Initialize();
        void Initialize(DataContext dbContext);
        void AddContent(DataContext dbContext);
        void Refresh();
    }
}