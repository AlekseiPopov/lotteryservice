﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotteryService.FrontEnd.Models
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly DataContext _dataContext;

        public DatabaseInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public DatabaseInitializer()
        {
            _dataContext = null;
        }

        public void Initialize()
        {
            _dataContext.Database.EnsureDeleted();
                _dataContext.Database.EnsureCreated();
                AddContent(_dataContext);
        }

        public void Initialize(DataContext dbContext)
        {
            dbContext.Database.EnsureCreated();
            AddContent(dbContext);
        }

        public void AddContent(DataContext dbContext)
        {
            dbContext.Departments.AddRange(DataSeed.Departments);
            dbContext.Employees.AddRange(DataSeed.Employees);
            dbContext.SaveChanges();
        }
        
        public void Refresh()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
        }
    }
}