using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using LotteryService.FrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotteryService.FrontEnd.Models.EntityConfigurations
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable("Employees");

            builder.HasKey(r => r.Id);
            builder.Property(r => r.Name).IsRequired().HasMaxLength(200);
            builder.Property(r => r.SecondName).IsRequired().HasMaxLength(200);
            builder.Property(r => r.ThirdName).IsRequired().HasMaxLength(200);
            builder.Property(r => r.Position).IsRequired().HasMaxLength(200);
            builder.Property(r => r.DepartmentId).IsRequired();
        }
    }
}