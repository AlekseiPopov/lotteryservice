using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotteryService.FrontEnd.Models;

namespace LotteryService.FrontEnd.Models
{
    public class Employee : BaseEntity
    {        
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string ThirdName { get; set; }
        public string PhotoPath { get; set; }
        public string Position { get; set; }
        public int PersonalNumber { get; set; }
        public Guid DepartmentId { get; set; }
        public Department Department { get; set; }
    }
}

