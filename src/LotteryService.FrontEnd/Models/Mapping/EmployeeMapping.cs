﻿using AutoMapper;
using LotteryService.FrontEnd.Models.DTO;

namespace LotteryService.FrontEnd.Models.Mapping
{
    public class EmployeeMapping : Profile
    {
        public EmployeeMapping()
        {
            CreateMap<Employee, EmployeeResponseDto>()
                .ForMember(e => e.Department, opt => opt.MapFrom(dto => dto.Department));
            CreateMap<CreateOrUpdateEmployeeDto, Employee>().ReverseMap();
                //.ForMember(e => e.DepartmentId, opt => opt.MapFrom(dto => dto.Department.Id));
                //CreateMap<Employee, CreateOrUpdateEmployeeDto>();
                //.ForMember(e => e.DepartmentId, opt => opt.MapFrom(dto => dto.Department.Id))
                //.ForMember(e => e.Department, opt => opt.MapFrom(dto => dto.Department));
        }
    }
}