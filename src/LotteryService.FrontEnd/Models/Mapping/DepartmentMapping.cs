﻿using LotteryService.FrontEnd.Models.DTO;
using AutoMapper;

namespace LotteryService.FrontEnd.Models.Mapping
{
    public class DepartmentMapping : Profile
    {
        public DepartmentMapping()
        {
            CreateMap<Department, DepartmentResponseDto>().ReverseMap();
            CreateMap<CreateOrUpdateDepartmentDto, Department>().ReverseMap();
        }
    }
}