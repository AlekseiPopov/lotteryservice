﻿using System;
using System.Collections.Generic;

namespace LotteryService.FrontEnd.Models
{
    public class DataSeed
    {
        private static string _employee1 = "1c7dab91-3317-41a5-adf5-8682fa3d12af";
        private static string _employee2 = "b296fb4f-e1fa-401b-907f-f28a88f47cf6";
        private static string _employee3 = "2ec1dcdc-b2b5-4a9e-8ace-33a8ae240f0f";
        private static string _employee4 = "1989e6d9-997d-445a-8233-3f57ac61765b";
        private static string _employee5 = "1989e6d9-997d-445a-8233-3f57ac617654";

        private static string _department1 = "4122d412-682a-4bad-9837-ceda4824e261";
        private static string _department2 = "21092523-4d24-4909-b774-751de633ed95";

        public static IEnumerable<Department> Departments => new List<Department>()
        {
            new Department()
            {
                Id = Guid.Parse(_department1),
                Name = "УАИМО"
            },
            new Department()
            {
                Id = Guid.Parse(_department2),
                Name = "ПДС"
            },
        };

        public static IEnumerable<Employee> Employees = new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse(_employee1),
                Name = "Василий",
                SecondName = "Васильев",
                ThirdName = "Васильевич",
                Position = "Инженер",
                PhotoPath = "",
                PersonalNumber = 703897,
                DepartmentId = Guid.Parse(_department1),
            },
            new Employee()
            {
                Id = Guid.Parse(_employee2),
                Name = "Петр",
                SecondName = "Петров",
                ThirdName = "Петрович",
                Position = "Слесарь КИПиА",
                PhotoPath = "",
                PersonalNumber = 703899,
                DepartmentId = Guid.Parse(_department1),
            },
            new Employee()
            {
                Id = Guid.Parse(_employee3),
                Name = "Иван",
                SecondName = "Иванов",
                ThirdName = "Иванович",
                Position = "Мастер КИПиА",
                PhotoPath = "",
                PersonalNumber = 703833,
                DepartmentId = Guid.Parse(_department1),
            },
            new Employee()
            {
                Id = Guid.Parse(_employee4),
                Name = "Александр",
                SecondName = "Александров",
                ThirdName = "Александрович",
                Position = "Диспетчер",
                PhotoPath = "",
                PersonalNumber = 703844,
                DepartmentId = Guid.Parse(_department2),
            },
            new Employee()
            {
                Id = Guid.Parse(_employee5),
                Name = "Артем",
                SecondName = "Артемов",
                ThirdName = "Артемович",
                Position = "Начальник смены",
                PhotoPath = "",
                PersonalNumber = 703811,
                DepartmentId = Guid.Parse(_department2),
            }
        };
    }
}