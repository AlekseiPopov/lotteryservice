﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace LotteryService.FrontEnd.Models.DTO
{
    public class CreateOrUpdateDepartmentDto
    {
        public Guid Id { get; set; }
        
        [Display(Name="Подразделение")]
        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string Name { get; set; }
    }
}