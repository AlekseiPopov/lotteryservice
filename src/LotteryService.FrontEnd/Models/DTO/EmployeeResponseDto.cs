﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LotteryService.FrontEnd.Models.DTO
{
    public class EmployeeResponseDto
    {
        public Guid Id { get; set; }
        [Display(Name="Имя")]
        public string Name { get; set; }
        [Display(Name="Фамилия")]
        public string SecondName { get; set; }
        [Display(Name="Отчество")]
        public string ThirdName { get; set; }
        [Display(Name="Должность")]
        public string Position { get; set; }
        
        [Display(Name="Табельный номер")]
        public int PersonalNumber { get; set; }
        public string PhotoPath { get; set; }
        [Display(Name="Подразделение")]
        public Department Department { get; set; }
    }
}
