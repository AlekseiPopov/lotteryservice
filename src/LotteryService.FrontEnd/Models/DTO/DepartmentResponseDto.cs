﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LotteryService.FrontEnd.Models.DTO
{
    public class DepartmentResponseDto
    {
        public Guid Id { get; set; }
        [Display(Name="Подразделение")]
        public string Name { get; set; }
    }
}
