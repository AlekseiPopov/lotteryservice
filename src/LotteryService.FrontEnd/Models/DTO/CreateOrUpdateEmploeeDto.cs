﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace LotteryService.FrontEnd.Models.DTO
{
    public class CreateOrUpdateEmployeeDto
    {
        public Guid Id { get; set; }
        
        [Display(Name="Имя")]
        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string Name { get; set; }
        
        [Display(Name="Фамилия")]
        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string SecondName { get; set; }
        
        [Display(Name="Отчество")]
        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string ThirdName { get; set; }
        
        [Display(Name="Должность")]
        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string Position { get; set; }
        
        [Display(Name="Табельный номер")]
        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        public int PersonalNumber { get; set; }
        
        public string PhotoPath { get; set; }
        public IFormFile Photo { get; set; }
        
        [Display(Name="Подразделение")]
        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        public Guid DepartmentId { get; set; }
   
    }
}   