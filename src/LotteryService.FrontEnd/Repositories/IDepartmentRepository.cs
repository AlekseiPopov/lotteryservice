﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LotteryService.FrontEnd.Models;

namespace LotteryService.FrontEnd.Repositories
{
    public interface IDepartmentRepository : IBaseRepository<Department>
    {
        Task<Department> GetDepartmentForUpdateAsync(Guid id);
    }
}
