﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using LotteryService.FrontEnd.Models;

namespace LotteryService.FrontEnd.Repositories
{
    public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(DataContext context) : base(context)
        {

        }
        
        public async Task<Employee> GetEmployeeForUpdateAsync(Guid id)
        {
            return await DbSet.Where(r => r.Id == id).Include(e => e.Department).FirstOrDefaultAsync();
        }

        public async Task<Employee> GetEmployeeByPersonalNumber(int personalNumber)
        {
            return await DbSet.Where(emp => emp.PersonalNumber == personalNumber).FirstOrDefaultAsync();
        }

        public async Task<Employee> GetEmployeeWithDepartmentByPersonalNumber(int id)
        {
            return await DbSet.AsNoTracking().Where(e => e.PersonalNumber == id).Include(p => p.Department).SingleOrDefaultAsync();
        }

        public async Task<Employee> GetEmployeeWithDepartmentById(Guid id)
        {
            return await DbSet.Include(e => e.Department).FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<ICollection<Employee>> GetAllEmployeesWithDepartments()
        {
            return await DbSet.Include(e=>e.Department).ToListAsync();
        }
    }
}
