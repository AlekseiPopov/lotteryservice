﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LotteryService.FrontEnd.Models;
using LotteryService.FrontEnd.Models.DTO;

namespace LotteryService.FrontEnd.Repositories
{
    public interface IEmployeeRepository : IBaseRepository<Employee>
    {
        //здесь будут добавлены специфические методы, характерные только для Работника (например сложные выборки с зависимостями и т.д.)
        Task<Employee> GetEmployeeForUpdateAsync(Guid id);
        Task<Employee> GetEmployeeByPersonalNumber(int personalNumber);
        Task<Employee> GetEmployeeWithDepartmentByPersonalNumber(int id);
        Task<Employee> GetEmployeeWithDepartmentById(Guid id);
        Task<ICollection<Employee>> GetAllEmployeesWithDepartments();
    }
}
