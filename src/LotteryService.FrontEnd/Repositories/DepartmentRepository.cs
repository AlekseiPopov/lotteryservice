﻿using System;
using Microsoft.EntityFrameworkCore;
using LotteryService.FrontEnd.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace LotteryService.FrontEnd.Repositories
{
    public class DepartmentRepository : BaseRepository<Department>, IDepartmentRepository
    {
        public DepartmentRepository(DataContext context) : base(context)
        {

        }

        public async Task<Department> GetDepartmentForUpdateAsync(Guid id)
        {
            return await DbSet.FirstOrDefaultAsync(r => r.Id == id);
        }
    }
}
