using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LotteryService.FrontEnd.Models.DTO;

namespace LotteryService.FrontEnd.Services
{
    public interface IDepartmentService
    {
        Task<ICollection<DepartmentResponseDto>> GetAllDepartments();
        Task<string> AddDepartmentAsync(CreateOrUpdateDepartmentDto department);
        Task RemoveDepartmentAsync(Guid id);
        Task UpdateDepartmentAsync(Guid id, CreateOrUpdateDepartmentDto department);
        Task<DepartmentResponseDto> GetDepartmentById(Guid id);
        Task<CreateOrUpdateDepartmentDto> GetDepartmentForUpdateById(Guid id);
    }
}