using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LotteryService.FrontEnd.Models.DTO;

namespace LotteryService.FrontEnd.Services
{
    public interface IEmployeeService
    {
        Task<ICollection<EmployeeResponseDto>> GetAllEmployees();
        Task<ICollection<EmployeeResponseDto>> GetAllEmployeesWithDepartments();
        Task<string> AddEmployeeAsync(CreateOrUpdateEmployeeDto employee);
        Task RemoveEmployeeAsync(Guid id);
        Task UpdateEmployeeAsync(Guid id, CreateOrUpdateEmployeeDto request);
        Task<EmployeeResponseDto> GetEmployeeById(Guid id);
        Task<CreateOrUpdateEmployeeDto> GetEmployeeForUpdate(Guid id);
        Task<EmployeeResponseDto> GetEmployeeByPersonalNumber(int personalNumber);
        Task<EmployeeResponseDto> GetEmployeeWithDepartmentById(Guid id);
    }
}