﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LotteryService.FrontEnd.Models;
using LotteryService.FrontEnd.Repositories;
using LotteryService.FrontEnd.Models.DTO;
using AutoMapper;

namespace LotteryService.FrontEnd.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IMapper _mapper;

        public DepartmentService(IDepartmentRepository departmentRepository, IMapper mapper)
        {
            _departmentRepository = departmentRepository;
            _mapper = mapper;
        }

        public async Task<string> AddDepartmentAsync(CreateOrUpdateDepartmentDto department)
        {
            if (department is null)
            {
                throw new AppException($"Department cannot be empty", 400);
            }

            var newDepartment = _mapper.Map<Department>(department);

            var newId = await _departmentRepository.CreateAsync(newDepartment);

            return $"Department was successfully created with id {newId}.";
        }

        public async Task<ICollection<DepartmentResponseDto>> GetAllDepartments()
        {
            var departments = await _departmentRepository.GetAllAsync();

            return _mapper.Map<List<DepartmentResponseDto>>(departments);
        }

        public async Task<DepartmentResponseDto> GetDepartmentById(Guid id)
        {
            var department = await _departmentRepository.GetByIdAsync(id);
            return _mapper.Map<DepartmentResponseDto>(department);
        }

        public async Task<CreateOrUpdateDepartmentDto> GetDepartmentForUpdateById(Guid id)
        {
            var department = await _departmentRepository.GetDepartmentForUpdateAsync(id);
            return _mapper.Map<CreateOrUpdateDepartmentDto>(department);
        }

        public async Task RemoveDepartmentAsync(Guid id)
        {
            var department = await _departmentRepository.GetByIdAsync(id);

            if (department == null)
            {
                throw new AppException($"Department with {id} wasn't found", 500);
            }

            await _departmentRepository.DeleteAsync(department);
        }

        public async Task UpdateDepartmentAsync(Guid id, CreateOrUpdateDepartmentDto department)
        {
            if (department is null)
            {
                throw new AppException($"Department cannot be empty", 400);
            }

            var existingDepartment = await _departmentRepository.GetDepartmentForUpdateAsync(id);

            if (existingDepartment is null)
            {
                throw new AppException($"Department with {id} doesn't exists", 400);
            }
            
            var updateDepartment = _mapper.Map(department, existingDepartment);

            await _departmentRepository.UpdateAsync(updateDepartment);
        }
    }
}