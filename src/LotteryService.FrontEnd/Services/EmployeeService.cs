﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LotteryService.FrontEnd.Repositories;
using LotteryService.FrontEnd.Models;
using LotteryService.FrontEnd.Models.DTO;

namespace LotteryService.FrontEnd.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IMapper _mapper;

        public EmployeeService(IEmployeeRepository employeeRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
        }

        public async Task<ICollection<EmployeeResponseDto>> GetAllEmployees()
        {
            var employees = await _employeeRepository.GetAllAsync();

            return _mapper.Map<List<EmployeeResponseDto>>(employees);
        }
        
        public async Task<ICollection<EmployeeResponseDto>> GetAllEmployeesWithDepartments()
        {
            var employees = await _employeeRepository.GetAllEmployeesWithDepartments();

            return _mapper.Map<List<EmployeeResponseDto>>(employees);
        }

        public async Task<string> AddEmployeeAsync(CreateOrUpdateEmployeeDto employee)
        {
            if (employee is null)
            {
                throw new AppException($"Request cannot be empty", 400);
            }

            var newEmployee = _mapper.Map<Employee>(employee);

            var newId = await _employeeRepository.CreateAsync(newEmployee);

            return $"Employee was successfully created with id {newId}.";
        }

        public async Task<EmployeeResponseDto> GetEmployeeById(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            return _mapper.Map<EmployeeResponseDto>(employee);
        }
        
        public async Task<CreateOrUpdateEmployeeDto> GetEmployeeForUpdate(Guid id)
        {
            var employee = await _employeeRepository.GetEmployeeForUpdateAsync(id);
            return _mapper.Map<CreateOrUpdateEmployeeDto>(employee);
        }

        public async Task<EmployeeResponseDto> GetEmployeeByPersonalNumber(int personalNumber)
        {
            var employee = await _employeeRepository.GetEmployeeWithDepartmentByPersonalNumber(personalNumber);
            return _mapper.Map<EmployeeResponseDto>(employee);
        }

        public async Task<EmployeeResponseDto> GetEmployeeWithDepartmentById(Guid id)
        {
            return  _mapper.Map<EmployeeResponseDto>(await _employeeRepository.GetEmployeeWithDepartmentById(id));
        }

        public async Task RemoveEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                throw new AppException($"Employee with {id} wasn't found", 500);
            }

            await _employeeRepository.DeleteAsync(employee);
        }

        public async Task UpdateEmployeeAsync(Guid id, CreateOrUpdateEmployeeDto employee)
        {
            if (employee is null)
            {
                throw new AppException($"Employee cannot be empty", 400);
            }

            var existingEmployee = await _employeeRepository.GetEmployeeForUpdateAsync(id);
            if (existingEmployee is null)
            {
                throw new AppException($"Employee with {id} doesn't exists", 400);
            }

            var updateEmployee = _mapper.Map(employee, existingEmployee);
            await _employeeRepository.UpdateAsync(updateEmployee);
        }
    }
}